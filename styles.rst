Styles Module
==================

The Styles module contains a parent class for styles and child classes for currently supported style factors.

:mod:`styles`
-----------------------

.. autoclass:: byuriskmodel.styles.style

.. autoclass:: byuriskmodel.styles.mktbeta 

.. autoclass:: byuriskmodel.styles.momentum

.. autoclass:: byuriskmodel.styles.size

.. autoclass:: byuriskmodel.styles.residual_volatility

.. autoclass:: byuriskmodel.styles.liquidity 

.. autoclass:: byuriskmodel.styles.nonlinear_beta 

.. autoclass:: byuriskmodel.styles.nonlinear_size
