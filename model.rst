Model Module
==================

:mod:`model`
-----------------------

.. autoclass:: byuriskmodel.model.risk_model


Usage
--------------

Initializing the model with basic stock data (:code:`sdb`), fundamentals data (:code:`fdb`), the existing 
exposures database (:code:`edb`), and benchmark constituent data (:code:`bdb`) will perform the following upon instantiation:

* Run initial filtering from the selected filter class (defaults to filtering to common equity and removing data points marked by CRSP as errors)
* Update exposures database with selected factors if there is more recent data in the stock data passed as an argument than in the passed exposures database object
* Run data replacement algorithms to replace missing factor exposures 
* Run secondary filtering to estimation universe (illiquidity filter)

::

	>>> from byuriskmodel.model import risk_model
	>>> m = risk_model(sdb,fdb,edb,bdb,model_type='custom',style_factors=['mktbeta','momentum','size','nonlinear_size'])

Once the model object is created, you can run any of the methods and access the updated model attributes.

:: 

	>>> myexposures = m.exposures_db # updated exposures db
	>>> myestu = m.estu_exposures_db # estimation universe exposures db 

	Generate factor returns and access the attribute
	>>> m.gen_factors()
	>>> myfactors = m.factor_returns

	Predict VCV and access the attribute
	>>> m.predict_VCV()
	>>> myVCV = m.VCV

	Predict SV and access the attribute
	>>> m.predict_specific_vol()
	>>> mySV = m.SV

	Analyze a portfolio
	>>> weights = pd.Series([0.3,0.7],index=[10026,10037])
	>>> exposures,variance,er = m.analyze_portfolio(weights)

This returns the exposures of the portfolio to the given factors, the predicted variance of the portfolios (Barra long and short horizons), and the expected return of the portfolio.

Portfolio optimization based on a benchmark is also an available method. 

::

	>>> hstar, wstar = m.portfolio_optimization(alpha, horizon='L', benchmark='SP500', active_risk_target = 0.1, risktolerance=1)
	
This takes an alpha vector (indexed by security ID), a Barra prediction horizon, an available benchmark (i.e. :code:`'SP500'`, :code:`'Russell3000'`, :code:`'rf'` for risk-free, etc.), an active risk target, 
and a risk tolerance parameter (defaults to one) as inputs and outputs vectors (indexed by security ID) of optimal portfolio active weights and normal weights. Alpha is set to zero for any security 
included in the estimation universe but not included in the alpha vector.