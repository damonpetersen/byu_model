.. Finance Library documentation master file, created by
   sphinx-quickstart on Sun Sep 29 13:40:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

BYU Silver Fund Risk Model Documentation
===========================================

This project contains the risk model for the BYU Silver Fund. This documentation is not a source 
of econometric detail, but is rather a source of in depth documentation on the python code for the 
risk model. For detail on econometrics and intuition, see the Silver Fund Student Manual.

.. toctree::
   :maxdepth: 2
   :caption: Modules:
   
   model  
   industries
   model_validation
   filter
   styles

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
