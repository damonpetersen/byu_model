Filter Module
==================

The Filter module contains a parent class for security file filters and child classes for currently supported filters.

:mod:`filter`
-----------------------

.. autoclass:: byuriskmodel.filter.filter

.. autoclass:: byuriskmodel.filter.common_equity_illiquidity1


