Industries Module
===================

The Industries module contains a parent class for industry definitions, and currently contains only one child class for 49 industries defined by Fama and French (definitions pulled from Ken French's website during summer 2020).

:mod:`industries`
-----------------------

.. autoclass:: byuriskmodel.industries.industries

.. autoclass:: byuriskmodel.industries.fama_french_49_industries

