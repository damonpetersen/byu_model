import pandas as pd
import numpy as np
from tabulate import tabulate
import styles as styles_module
import industries as industries_module
import filter as filter_module
import model_validation as model_validation_module
import math

class risk_model:
    """    
    Main class for Silver Fund risk model. Under construction: more documentation to come.

    Parameters
    ----------

    stk_db: pandas.core.frame.DataFrame
        :code:`DataFrame` containing daily price, return, shares outstanding, volume, and security identifiers (SIC Code, CUSIP, BYU ID, etc.).
    fundamentals_db: pandas.core.frame.DataFrame
        :code:`DataFrame` containing fundamentals data for securities including 12-month forward earnings, trailing 12-month earnings, long-term earnings
        growth analyst forecasts, trailing 5 years earnings growth, trailing 5 years sales growth, trailing 12-month dividend, book-value of common equity, 
        book-value of preferred equity, book value of long-term debt, book-value of total debt, and book value of total assets.
    exposures_db: pandas.core.frame.DataFrame
        :code:`DataFrame` containing historical factor exposures (None if calculating historical factor exposures from scratch). An error will be thrown if 
        the contents of :code:`exposures_db` do not match the style and industry factor specifications in from :code:`model_type` and :code:`ind`.
    benchmark_db: pandas.core.frame.DataFrame
        :code:`DataFrame` containing historical constituent data for selected benchmarks (currently Russell 3000 and S&P 500).
    ind: str
        Defaults to 'fama_french_49_industries', which is currently the only available industry definition scheme. Future definition schemes may be developed 
        for other industry definitions based on SIC code, GICS code, etc.
    model_type: str
        Defaults to :code:`'USE4'` which includes industry factors as well as all style factors defined in the Barra USE4 model. Other available model types currently 
        include :code:`'USE4_nofundamentals'` which includes all Barra USE4 style factors that do not require data from corporate financial statements. Future development
        will likely include options for the style factors in the Barra GEM model. :code:`'custom'` is also an option. If :code:`'custom'` is selected, the user inputs 
        a list of string names for style factors in the :code:`style_factors` argument.
    filter: str
        String name of the filter class in the filter module to use as the filter for the model. Defaults to 'common_equity_illiquidity1'.
    quiet: bool
        Whether or not to suppress updates to standard ouput. Defaults to False.
    style_factors: None or list(str)
        Only used if :code:`model_type` is set to :code:`'custom'`. List of string style factors to use in a custom model.
   
    Methods
    -------
    gen_factors()
        Return factor returns and factor portfolio compositions (portfolio weights).
    predict_VCV()
        Predict and return factor covariance matrix via Newey-West exponential weighted methodology. Returns dictionary with keys 'L' and 'S' for 
        long- or short-term predictions (corresponding to USE4S and USE4L prediction horizons in Barra methodology).
    predict_specific_vol()
        Predict a diagonal matrix of specific volatilities by security. Returns dictionary with keys 'L' and 'S' for long- or short-term predictions (corresponding to 
        USE4L and USE4S prediction horizons in Barra methodology).
    analyze_portfolio(portfolio_weights, time_series_historical=False)
        Return volatility forecasts, expected returns, and factor exposures for a portfolio defined in :code:`portfolio_weights`. If :code:`time_series_historical` is 
        specified, historical forecasts and exposures will be returned in addition to future forecasts and current exposures (requires a historical time-series of portfolio weights).
        Return object is a tuple of the form tuple(exposures,variance,expected_return) where exposures is a :code:`pandas` :code:`Series` with factor exposures of the portfolio, 
        variance is a dictionary with portfolio predicted volatility (variance of return) indexed by keys 'L' and 'S' for long- or short-term predictions (corresponding to USE4L and 
        USE4S prediction horizons in Barra methodology), and expected_return is a dictionary indexed in the same manner.
    portfolio_optimization(ratio='information', length='L', historical_periods=0)
        TBD. Constrained optimization of information ratio (and perhaps also of sharpe ratio). If :code:`historical_periods` is greater than zero, historical optimized portfolios will 
        be returned for the specified number of periods in addition to the current optimized portfolio. See :code:`predict_VCV` for a description of the :code:`length` parameter.
    
    Notes
    ------
    Filtering, data-replacement, and updating the exposures database (if :code:`stk_db` contains more recent data than :code:`exposures_db`) is completed upon initialization of the model object.

    """
    
    
    def __init__(self,stk_db,fundamentals_db,exposures_db,benchmark_db,ind='fama_french_49_industries',model_type='USE4',filter='common_equity_illiquidity1',quiet=False,style_factors=None):
        self.stk_db = stk_db 
        self.fundamentals_db = fundamentals_db
        self.exposures_db = exposures_db
        self.benchmark_db = benchmark_db
        self.quiet = quiet
        self.custom_style_factors = style_factors
        self._assign_attributes(ind,filter,model_type)
        self._assertions()
        self._initial_filter()
        self._initial_data_replacement()
        self._update_exposures()
        self._secondary_filter()
        self._secondary_data_replacement()
        self.VCV = {}
        self.SV = {}
        
    def _assign_attributes(self,ind,filter,model_type):
        if not self.quiet:
            print("Initializing style, filter, and industry classes")
        available_industries = ['fama_french_49_industries']
        available_filters = ['common_equity_illiquidity1']
        available_model_types = ['USE4','USE4_nofundamentals','custom']
        available_style_factors = ['mktbeta','momentum','size','nonlinear_beta','nonlinear_size','liquidity','residual_volatility']
        assert ind in available_industries, "ind must be one of the available industry definition schemes: {}".format(available_industries)
        assert filter in available_filters, "filter must be one of the available filters: {}".format(available_filters)
        assert model_type in available_model_types, "model_type must be one of the available model types: {}".format(available_model_types)
        if model_type == 'custom':
            for s in self.custom_style_factors:
                assert (s in available_style_factors), "style {} is not available".format(s)
            if ('nonlinear_beta' in self.custom_style_factors) & ('mktbeta' not in self.custom_style_factors):
                raise ValueError("mktbeta must be included in custom style factors if nonlinear_beta is specified")
            if ('nonlinear_size' in self.custom_style_factors) & ('size' not in self.custom_style_factors):
                raise ValueError("size must be included in custom style factors if nonlinear_size is specified")
            if ('residual_volatility' in self.custom_style_factors) & ('mktbeta' not in self.custom_style_factors):
                raise ValueError("mktbeta must be included in custom style factors if residual_volatility is specified")

        if ind == 'fama_french_49_industries':
            self.industries = industries_module.fama_french_49_industries()
        if filter == 'common_equity_illiquidity1':
            self.filter = filter_module.common_equity_illiquidity1()
        if model_type == 'USE4':
            raise ValueError("USE4 is not configured yet. The USE4 model type is expected to be available shortly after Worldscope data is available to the Silver Fund.")
        elif model_type == 'USE4_nofundamentals':
            self.styles = []
            self.styles.append(styles_module.mktbeta())
            self.styles.append(styles_module.momentum())
            self.styles.append(styles_module.size())
            self.styles.append(styles_module.residual_volatility())
            self.styles.append(styles_module.liquidity())
            self.styles.append(styles_module.nonlinear_beta())
            self.styles.append(styles_module.nonlinear_size())
        elif model_type == 'custom':
            self.styles = []
            if 'mktbeta' in self.custom_style_factors: 
                self.styles.append(styles_module.mktbeta())
            if 'size' in self.custom_style_factors:
                self.styles.append(styles_module.size())
            if 'momentum' in self.custom_style_factors:
                self.styles.append(styles_module.momentum())
            if 'residual_volatility' in self.custom_style_factors:
                self.styles.append(styles_module.residual_volatility())
            if 'liquidity' in self.custom_style_factors:
                self.styles.append(styles_module.liquidity())
            if 'nonlinear_beta' in self.custom_style_factors:
                self.styles.append(styles_module.nonlinear_beta())
            if 'nonlinear_size' in self.custom_style_factors:
                self.styles.append(styles_module.nonlinear_size())
                
        if 'mcap' not in list(self.stk_db.columns): 
            self.stk_db['mcap'] = self.stk_db.prc * self.stk_db.shr * 1000.
        self.available_benchmarks = ['SP500','Russell3000']

                
    def _assertions(self):
        assert isinstance(self.stk_db,pd.core.frame.DataFrame), "stk_db must be a pandas dataframe"
        assert isinstance(self.fundamentals_db,pd.core.frame.DataFrame), "fundamentals_db must be a pandas dataframe"
        assert isinstance(self.benchmark_db,pd.core.frame.DataFrame), "benchmark_db must be a pandas dataframe"
        assert all([(s in list(self.benchmark_db.columns)) for s in ['permno','caldt','benchmark']]), "benchmark_db must have caldt, permno, and benchmark columns"
        assert all([(s in list(self.benchmark_db.benchmark.unique())) for s in self.available_benchmarks]), "All available benchmarks ({}) must be included in benchmark_db".format(self.available_benchmarks)
        assert all([isinstance(s,styles_module.style) for s in self.styles]), "styles must be a list of styles.style from the styles module" # parent-child assertion instance stuff -- debug
        assert isinstance(self.industries,industries_module.industries), "industries must be an industries.industries class from the industries module" # parent-child assertion instance stuff -- debug
        assert isinstance(self.filter,filter_module.filter), "filter must be a filter.filter class from the filter module"
        assert all([(s.__class__.__name__ in list(self.exposures_db.columns)) for s in self.styles]), "all styles specified in class instantiation must be present in exposures_db"
        assert isinstance(self.quiet,bool), "quiet must be boolean"
    
    def _initial_filter(self):
        if not self.quiet:
            print("Initial filtering")
        self.stk_db, self.fundamentals_db = self.filter.initial_filter(self.stk_db,self.fundamentals_db)
    
    def _secondary_filter(self):
        # estu: estimation universe
        if not self.quiet:
            print("Secondary filtering")
        self.estu_stk_db, self.estu_fundamentals_db, self.estu_exposures_db = self.filter.secondary_filter(self.stk_db,self.fundamentals_db,self.exposures_db)
        
    def _initial_data_replacement(self):
        # TODO: This needs to replace all NAN in fundamentals_db and stk_db to the point that linear algebra routines will have no issues generating style and industry exposures  
        #       Can be from forward filling within reason, filtering out NANs if necessary further than the filter class specifies, etc.
        if not self.quiet:
            print("Initial data replacement")
        pass
        
    def _secondary_data_replacement(self):
        # TODO: This needs to replace all NAN in exposures_db to the point that linear algebra routines will have no issues generating VCV and factor returns from exposures_db 
        #       This will probably use a structural model similar to the specific return structural model to generate exposures for securities with missing exposures by assuming 
        #       that a missing exposure will be similar to the exposures of similar stocks.
        
        if not self.quiet:
            print("Secondary data replacement")
        #for xvar in self.xvarlist:
        #    self.estu_exposures_db = self.estu_exposures_db.query("{} == {}".format(xvar,xvar))  
        # Note: This is simply wrong -- and is only here as a placeholder 
        replacementdf = pd.DataFrame(np.random.random(self.estu_exposures_db.shape),columns=self.estu_exposures_db.columns,index=self.estu_exposures_db.index)
        self.estu_exposures_db = self.estu_exposures_db.fillna(replacementdf)
        replacementdf = pd.DataFrame(np.random.random(self.estu_stk_db.shape),columns=self.estu_stk_db.columns,index=self.estu_stk_db.index)
        self.estu_stk_db = self.estu_stk_db.fillna(replacementdf)
    
    def _update_exposures(self):
        # TODO: Make this runs only for dates that are not already in the self.exposures_db
        recent_stk_db = self.stk_db.caldt.max()
        recent_exposures_db = self.exposures_db.caldt.max()
        self.indlist = list(self.industries.indlist) 
        self.xvarlist = [s.__class__.__name__ for s in self.styles] + self.indlist + ['country']
        if recent_exposures_db < recent_stk_db:
            try:
                relmin = list(self.stk_db.caldt.sort_values().unique())[-600]
            except IndexError:
                raise IndexError("Input stk_db is too short. There are not enough dates in the time series.")
            rel = self.stk_db.query("caldt > '{}'".format(relmin)).copy()
            rel2 = self.fundamentals_db.query("caldt > '{}'".format(relmin)).copy()
            if not self.quiet:
                print("Updating exposures")
            exposures = self.industries.form_industries(rel)
            #self.indlist = list(exposures.columns)
            #self.indlist.remove('country')
            for s in self.styles:
                if not self.quiet:
                    print("Updating {}".format(s.__class__.__name__))
                if s.__class__.__name__ == 'nonlinear_beta':
                    exposures = exposures.merge(s.generate_exposures(rel,rel2,extra=exposures[['mktbeta','caldt','permno']]),on=['permno','caldt'],how='outer')
                elif s.__class__.__name__ == 'nonlinear_size':
                    exposures = exposures.merge(s.generate_exposures(rel,rel2,extra=exposures[['size','caldt','permno']]),on=['permno','caldt'],how='outer')
                elif s.__class__.__name__ == 'residual_volatility':
                    exposures = exposures.merge(s.generate_exposures(rel,rel2,extra=exposures[['mktbetaresid','caldt','permno']]),on=['permno','caldt'],how='outer')
                else:
                    exposures = exposures.merge(s.generate_exposures(rel,rel2),on=['permno','caldt'],how='outer') 
            self.exposures_db = pd.concat([self.exposures_db,exposures.loc[exposures.caldt > recent_exposures_db]])
            #self.exposures_db = self.exposures_db.merge(exposures.loc[exposures.caldt > recent_exposures_db],how='outer',on=['permno','caldt'])
            
    def gen_factors(self):
        if not self.quiet:
            print("Generating factor returns")
        df = self.estu_exposures_db.merge(self.estu_stk_db[['mcap','permno','caldt','ret']],on=['permno','caldt'])
        self.factor_returns = df.groupby('caldt').apply(self._constrained_ols,self.xvarlist,self.indlist).unstack()
        self.factor_returns.columns = self.factor_returns.columns.droplevel()
        self.factor_returns = self.factor_returns.drop('lambda',axis=1)
        return self.factor_returns
        
    def predict_specific_vol(self):
        if not self.quiet:
            print("Predicting specific volatility")
        # generate factor returns if they have not already been generated
        if not hasattr(self,'factor_returns'): 
            self.gen_factors()
        df = self.estu_exposures_db.merge(self.estu_stk_db,on=['permno','caldt'])
        df = df.merge(self.factor_returns,on=['caldt'],suffixes=('_exposure','_return'))
        df['u'] = df.ret.copy()
        for xvar in self.xvarlist:
            df.u = df.u - df['{}_exposure'.format(xvar)] * df['{}_return'.format(xvar)]
        for l in ['S','L']:
            ###########################################
            #       Assign half-life 
            ###########################################
            if l == 'S':
                svhl = 84  # specific volatility half-life
            elif l == 'L':
                svhl = 252 # factor volatility half-life
            sv = df.groupby('permno').apply(self._specific_vol_a,svhl)
            sv = self._specific_structural(sv)
            self.SV[l] = pd.DataFrame(np.diag(sv.sv),columns=sv.index,index=sv.index)
        return self.SV
        
        
    def _specific_structural(self,sv):
        if not self.quiet:
            print("Specific volatility structural model")
        sv = pd.DataFrame(sv)
        sv.columns = ['sv']
        recent = self.estu_exposures_db.caldt.max()
        svts = sv.query("sv == sv").copy()
        svts['logsv'] = np.log(svts.sv.astype(float))
        svts = svts.sort_values(by='permno')
        svts['permno'] = svts.index
        tss = svts.permno.unique() # time-series securities
        X_ = self.estu_exposures_db.query("(caldt == '{0}') & (permno in {1})".format(recent,list(tss))).sort_values(by=['caldt','permno'])[list(self.xvarlist)+['permno']]
        usablepermno = list(X_.permno.unique())
        X_.index = X_.permno
        X_ = X_.drop('permno',axis=1)
        X = X_.to_numpy() 
        b = np.matmul(np.linalg.inv(np.matmul(X.T,X)),np.matmul(X.T,svts.query("permno in {}".format(usablepermno))['logsv'].to_numpy()))
        sv['permno'] = sv.index
        strs = set(sv.permno.unique()).difference(set(svts.permno.unique())) # structural securities 
        X2_ = self.estu_exposures_db.query("(caldt == '{0}') & (permno in {1})".format(recent,list(strs))).sort_values(by=['caldt','permno'])[list(self.xvarlist)+['permno']]
        usablepermno2 = list(X2_.permno.unique())
        X2_.index = X2_.permno
        X2_ = X2_.drop('permno',axis=1)
        X2 = X2_.to_numpy()
        sigmastr = np.exp(np.matmul(X2,b))
        sigmastr = pd.DataFrame(sigmastr,columns=['sv'],index=pd.Series(usablepermno2).sort_values())
        sv = sv.fillna(sigmastr) 
        return sv
        
    @staticmethod
    def _lags_by_halflife(hl, d_param = 1e-5):
        #hl: half-life parameter tau; d_papram = convergence cut-off
        #return the efficient number of lags
        w = - (hl * np.log(d_param)) / np.log(2)
        return math.ceil(w)
    
    def _specific_vol_a(self,g,hl):
        #n = self._lags_by_halflife(hl)
        n = 252 * 2 # use 2 years of data rather than efficient lag since efficient lag is too long for data purposes
        w = self._exponential_weights(n,hl)
        g = g.sort_values(by='caldt').reset_index(drop=True)
        grel = g.iloc[-n:,:]
        ubar = grel.u.mean()
        grel['u2'] = grel.u - ubar
        grel['u2'] = np.power(grel.u2,2.0)
        if w.shape[0] > grel.shape[0]:
            sigma = np.nan
        else:
            sigma = np.matmul(w.to_numpy().T,np.reshape(grel.u2.to_numpy(),(n,1)))[0,0]
        return sigma
    
    @staticmethod
    def _exponential_weights(n,tau):
        # old to recent
        lamb = np.power(0.5,1./tau)
        df = pd.DataFrame(np.nan,index=np.arange(n),columns=['w'])
        df['T-t'] = n-pd.Series(df.index.to_list())
        df['w'] = np.power(lamb,df['T-t'])
        df['w'] = df['w'] / df.w.sum()
        return df[['w']]

    
    def predict_VCV(self):
        if not self.quiet:
            print("Predicting factor covariance matrix")
        # generate factor returns if they have not already been generated
        if not hasattr(self,'factor_returns'): 
            self.gen_factors()
        
        for l in ['S','L']:
            ###########################################
            #       Assign half-lives and lags
            ###########################################
            if l == 'S':
                fvhl = 84  # factor volatility half-life
                nwvl = 5   # newey-west volatility lags
                fchl = 504 # factor correlation half-life
                nwcl = 2   # newey-west correlation lags
            elif l == 'L':
                fvhl = 252 # factor volatility half-life
                nwvl = 5   # newey-west volatility lags
                fchl = 504 # factor correlation half-life
                nwcl = 2   # newey-west correlation lags
                
            corrcov = self._cov_matrix(self.factor_returns,fchl,nwcl)
            volcov = self._cov_matrix(self.factor_returns,fvhl,nwvl)
            k = len(self.factor_returns.columns)
            retval = np.zeros((k,k))
            for i in range(k):
                for j in range(k):
                    retval[i,j] = corrcov[i,j] / (np.sqrt(corrcov[i,i]) * np.sqrt(corrcov[j,j]) ) * np.sqrt(volcov[i,i]) * np.sqrt(volcov[j,j])
            self.VCV[l] = pd.DataFrame(retval,index=self.factor_returns.columns,columns=self.factor_returns.columns)
        return self.VCV
    
    def analyze_portfolio(self, portfolio_weights, time_series_historical=False):
        # portfolio_weights should be indexed by permno and have the same permno as the index for self.SV's entries
        # TODO: time_series_historical is not supported yet: future development would be to support 
        #       a time-series of portfolio weights to run portfolio analysis on a strategy (strategy 
        #       defined as a time series of portfolio weights)
        if not self.quiet:
            print("Analyzing portfolio")
        if ('S' not in self.SV.keys()) or ('L' not in self.SV.keys()):
            self.predict_specific_vol()
        if ('S' not in self.VCV.keys()) or ('L' not in self.VCV.keys()):
            self.predict_VCV()
        assert isinstance(portfolio_weights,pd.core.series.Series), "portfolio_weights must be a series"
        portfolio_weights = portfolio_weights.sort_index() # need index to be named for this to work
        recent = self.estu_exposures_db.caldt.max()
        permnoinput = portfolio_weights.index
        permnoexisting = self.SV['S'].sort_index().index
        permnoexisting = self.estu_exposures_db.query("(permno in {0}) & (caldt=='{1}')".format(list(permnoexisting),recent)).sort_values(by='permno').permno
        assert permnoinput.isin(permnoexisting).all(), "the securities indexing portfolio_weights must be included in the estimation universe (in the risk_model.SV object and in the most recent date in estu_exposures_db)."
        #assert permnoinput == permnoexisting, "the securities indexing portfolio_weights must be the same as the securities indexing the specific volatility object: model.SV entries"
        n = len(permnoexisting)
        # add zero weight in portfolio_weights for those permno in SV that don't have entries in portfolio_weights 
        portw = pd.Series(np.nan,index=permnoexisting).sort_index()
        for idx,val in portw.iteritems():
            if idx in portfolio_weights.index:
                portw[idx] = portfolio_weights[idx]
            else:
                portw[idx] = 0
        w = np.reshape(portw.sort_index().to_numpy(),(1,n))
        X_ = self.estu_exposures_db.query("(permno in {0}) & (caldt=='{1}')".format(list(permnoexisting),recent)).sort_values(by='permno')[list(self.xvarlist)]
        X = X_.to_numpy()
        exposures_ = np.matmul(w,X)
        exposures = pd.DataFrame(exposures_.T,index=X_.columns,columns=['exposures']).sort_index()
        k = exposures.shape[0]
        er = {}
        variance = {}
        for l in ['S','L']:
            VCV_ = self.VCV[l]
            VCV_ = VCV_.reindex(sorted(VCV_.columns), axis=1).reindex(sorted(VCV_.index), axis=0)
            VCV = VCV_.to_numpy()
            exp_ = np.reshape(exposures.to_numpy(),(1,k))
            #SV_ = self.SV[l].query("permno in {}".format(list(permnoexisting))).loc[list(permnoexisting)]
            SV_ = self.SV[l].loc[list(permnoexisting),list(permnoexisting)]
            SV_ = SV_.reindex(sorted(SV_.columns),axis=1).reindex(sorted(SV_.index),axis=0)
            SV = SV_.to_numpy()
            temp = np.matmul(np.matmul(exp_,VCV),exp_.T) + np.matmul(w,np.matmul(SV,w.T))
            variance[l] = temp[0,0]
            fr_ = self.factor_returns.query("caldt == '{}'".format(recent))
            fr_= fr_.reindex(sorted(fr_.columns),axis=1)
            fr = fr_.to_numpy()
            er[l] = np.inner(exp_,fr)[0,0]
        return exposures,variance,er
            
        
    def _get_benchmark_weights(self,benchmark,permnoexisting):
        maxdate = self.benchmark_db.caldt.max()
        rel = self.benchmark_db.query("(caldt == '{0}') & (benchmark == '{1}')".format(maxdate,benchmark))
        securities = list(rel.permno.unique())
        mcaps = self.stk_db.query("(caldt == '{0}') & (permno in {1})".format(maxdate,securities))[['permno','mcap']]
        mcaps.index = mcaps.permno
        mcapsum = mcaps.mcap.sum()
        portw = pd.Series(np.nan,index=permnoexisting).sort_index()
        for idx,val in portw.iteritems():
            if idx in securities:
                portw[idx] = mcaps.loc[idx,'mcap'] / mcapsum
            else:
                portw[idx] = 0
        return portw
        
    def portfolio_optimization(self, alpha, horizon = 'L', benchmark = 'SP500', active_risk_target = 0.1, risktolerance = 1):
        #TODO: Add translation function that translates expected returns from students and from Barra model to benchmark alphas (will require benchmark)
        assert benchmark in self.available_benchmarks + ['rf'], "benchmark must be either 'rf' for risk-free rate or in {}".format(self.available_benchmarks)
        if ('S' not in self.SV.keys()) or ('L' not in self.SV.keys()):
            self.predict_specific_vol()
        if ('S' not in self.VCV.keys()) or ('L' not in self.VCV.keys()):
            self.predict_VCV()
        if not self.quiet:
            print("Optimizing portfolio")
        assert isinstance(alpha,pd.core.series.Series), "alphas must be a series"
        alpha = alpha.sort_index()
        recent = self.estu_exposures_db.caldt.max()
        permnoinput = alpha.index
        permnoexisting = self.SV['S'].sort_index().index
        permnoexisting = self.estu_exposures_db.query("(permno in {0}) & (caldt=='{1}')".format(list(permnoexisting),recent)).sort_values(by='permno').permno
        assert permnoinput.isin(permnoexisting).all(), "the securities indexing alphas must be included in the estimation universe (in the risk_model.SV object and in the most recent date in estu_exposures_db)."
        n = len(permnoexisting)
        # add zero weight in portfolio_weights for those permno in SV that don't have entries in portfolio_weights 
        portw = pd.Series(np.nan,index=permnoexisting).sort_index()
        for idx,val in portw.iteritems():
            if idx in alpha.index:
               portw[idx] = alpha[idx]
            else:
               portw[idx] = 0
        a = np.reshape(portw.sort_index().to_numpy(),(1,n))
        expos_ = self.estu_exposures_db.query("(permno in {0}) & (caldt=='{1}')".format(list(permnoexisting),recent)).sort_values(by='permno')[list(self.xvarlist)]
        expos = expos_.to_numpy()
        VCV_ = self.VCV[horizon]
        VCV_ = VCV_.reindex(sorted(VCV_.columns), axis=1).reindex(sorted(VCV_.index), axis=0)
        VCV = VCV_.to_numpy()
        SV_ = self.SV[horizon].loc[list(permnoexisting),list(permnoexisting)]
        SV_ = SV_.reindex(sorted(SV_.columns),axis=1).reindex(sorted(SV_.index),axis=0)
        SV = SV_.to_numpy()
        C = np.zeros((1,len(SV))) #just used a vector of zeros for now, customize later.
        c = np.zeros((1,len(SV)))
        V = np.matmul(np.matmul(expos,VCV),expos.T) + SV
        vinv = np.linalg.inv(V)
        cvc_inv = np.matmul(np.matmul(C,vinv),C.T)
        M = np.eye(len(SV)) - np.matmul(np.matmul(C.T, cvc_inv), np.matmul(C, vinv))
        zero_equal = (2 * risktolerance) ** -1 * np.matmul(np.matmul(vinv, M), a.T)
        general_equal = 2 * risktolerance * np.matmul(cvc_inv, c)
        hstar = zero_equal - general_equal.T
        hstar_scaled = (active_risk_target / np.sqrt(np.matmul(np.matmul(hstar.T, V), hstar))) * hstar
        hstar_scaled = pd.DataFrame(hstar_scaled,columns=['hstar'],index=permnoexisting)['hstar']
        if benchmark != 'rf':
            benchmark_weights = self._get_benchmark_weights(benchmark,permnoexisting)
        else:
            benchmark_weights = np.zeros(hstar.shape)
        w_star = benchmark_weights + hstar_scaled  
        return hstar_scaled, w_star

    
    @staticmethod
    def _constrained_ols(g,xvarlist,indlist):
        totmcap = g.mcap.sum()
        capwtind = []
        for ind in indlist:
            capwtind.append((g[ind]*g.mcap).sum()/totmcap)
        g['mcapsv'] = np.sqrt(g.mcap)
        # ensure no NAN are produced in square root procedure
        g = g.query("mcapsv == mcapsv")
        mcaps = np.diag(g.mcapsv)
        myx = np.array(g[xvarlist],dtype=float)
        myy = np.matmul(np.matmul(myx.T,mcaps),np.array(g.ret,dtype=float))
        myxx = np.matmul(np.matmul(myx.T,mcaps),myx)
        stylenum = len(xvarlist) - len(indlist) - 1
        cols = np.concatenate((myxx,np.array([[0]+capwtind+[0]*stylenum],dtype=float)),axis=0)
        cols = np.hstack([cols,np.array([[0]+capwtind+[0]*(stylenum+1)]).T])
        yy = np.concatenate((myy,[[0]]),axis=None)
        b = np.linalg.solve(cols,yy)
        b = pd.DataFrame(b,index=xvarlist+['lambda'],columns=['Factor'])
        #assert (b.loc[indlist,'Factor']*capwtind).sum() < 1.0e-12
        return b

                
    def _cov_matrix(self,factors,hl,lag):
        # factors is pandas dataframe with factors as columns and rows as time periods
        # hl is half-life
        # lag is number of newey-west lags 
        factorcols = factors.columns
        k = len(factorcols)
        factors = factors.reset_index(drop=True)
        zbar = factors.mean().to_numpy()
        lamb = np.power(0.5,1./hl)
        T = factors.shape[0]
        factors['T-t'] = T-pd.Series(factors.index.to_list())
        factors['w'] = np.power(lamb,factors['T-t'])
        factors['w'] = factors['w'] / factors.w.sum()
        factorsnp = factors[factorcols].to_numpy()
        gamma0 = np.zeros((k,k))
        for t in range(T):
            gamma0 = np.add(gamma0,self._element1(t,0,factorsnp,zbar,factors.loc[t,'w']))
        gammas = {}
        for v in range(lag):
            gammas[v] = np.zeros((k,k))
            for t in range(v,T):
                gammas[v] = np.add(gammas[v],self._element1(t,v+1,factorsnp,zbar,factors.loc[t,'w']))
        S = gamma0
        for v in range(lag):
            S = np.add(S,(1-(v+1)/(lag+1))*np.add(gammas[v],gammas[v].T))
        return S
                
    @staticmethod
    def _element1(t,v,factors,zbar,w):
        k = len(zbar)
        return w * np.matmul(np.reshape((factors[t,:] - zbar),(k,1)),np.reshape((factors[t-v,:] - zbar),(1,k)))
        
            
        