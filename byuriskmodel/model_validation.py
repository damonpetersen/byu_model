import pandas as pd
import numpy as np

class model_validation:
    """    
    Model validation class for Silver Fund risk model. Under construction: more documentation to come.

    Parameters
    ----------

    model: byuriskmodel.model.RiskModel
        Risk model instance to validate.
    data: pandas.core.frame.DataFrame or None
        Additional data to use for validation if desired. 
   
    Methods
    -------
    VCV_validation(tau,lag,periods)
        Successively simulate factor returns from known EWMA covariance process, and check 
        model's ability to retrieve / predict true future covariances from simulated prior factor returns. 
        Will return tables / graphs as appropriate. 
    sub_sampling(data='RiskModel',segment=0.2)
        Run time-series (remove odd months from odd years, even months from even years) sub-sampling 
        validation and cross-sectional (remove representative cross-sectional :code:`segment`) sub-sampling. 
        :code:`data` specifies whether to use data from 'RiskModel' instance or from 'ModelValidation' instance.

    """
    
    def __init__(self):
        print('hello')