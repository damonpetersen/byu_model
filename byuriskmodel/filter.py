import pandas as pd
import numpy as np
from tabulate import tabulate
from scipy.stats import percentileofscore
import io


class filter():
    """    
    Base filter class for input to :code:`byuriskmodel.model.risk_model`. 
    
    Parameters
    ----------

    None. 
        Contains methods to perform initial filtering (prior to exposures calculation) 
        and secondary filtering (prior to VCV and specific volatility estimation).
    
    Methods
    --------
    
    initial_filter
        Initial filtering prior to exposures estimation.
    secondary_filter
        Secondary filtering prior to VCV and specific volatility predictions.
    
    """

    def __init__(self):
        self.stk_queries = []
        self.fundamentals_queries = []
        
    def _assertions(self,stk_db,fundamentals_db):
        assert isinstance(stk_db,pd.core.frame.DataFrame), "stk_db must be a pandas dataframe"
        assert isinstance(fundamentals_db,pd.core.frame.DataFrame)
        for c in ['caldt','ret','siccd','prc','shr','rf','permno']:
            assert c in list(stk_db.columns), "{} must be in stk_db".format(c)
        #for c in []
        #    assert c in list(fundamentals_db.columns), "{} must be in fundamentals_db".format(c)
        s = self._prints(stk_db.caldt.dtype)
        assert 'datetime64' in s, "caldt must be datetime type"
            
        
    def initial_filter(self,stk_db,fundamentals_db):
        self._assertions(stk_db,fundamentals_db)
        stk_db.prc = np.abs(stk_db.prc)
        self._initial_query_specification()
        # Note: These initial filters need to be strong enough to avoid NAN in linear algebra routines for exposures calculations.
        #       These initial filters + data-replacement methods in byuriskmodel.model.risk_model need to be strong enough to get all NAN out.
        for q in self.stk_queries:
            stk_db = stk_db.query(q)
        for q in self.fundamentals_queries:
            fundamentals_db = fundamentals_db.query(q)
        return stk_db,fundamentals_db
        
    def secondary_filter(self,stk_db,fundamentals_db,exposures_db):
        # TODO: add secondary filtering on fundamentals_db 
        estu_stk_db = self._illiquidity_specification(stk_db)
        # TODO: add secondary filtering based on coverage -- we want our estimation universe to have coverage on variables
        # TODO: add secondary filtering to exposures_db to ensure we don't have NAN
        estu_fundamentals_db = fundamentals_db
        estu_exposures_db = exposures_db
        return estu_stk_db, estu_fundamentals_db, estu_exposures_db
        
    @staticmethod
    def _prints(*args, **kwargs):
        output = io.StringIO()
        print(*args, file=output, **kwargs)
        contents = output.getvalue()
        output.close()
        return contents
        
class common_equity_illiquidity1(filter):   
    """    
    Filters to common equity in the United States and filters out illiquid 
    stocks based on monthly trading volume (dollar value of trading volume). 
    All securities with monthly dollar value trading volume in the prior month 
    beneath the percentile represented by 5M USD / month in December 2019
    are filtered out.
    
    Parameters
    ----------

    None. 
        Contains methods to perform initial filtering (prior to exposures calculation) 
        and secondary filtering (prior to VCV and specific volatility estimation).
    
    Methods
    --------
    
    initial_filter
        Initial filtering prior to exposures estimation.
    secondary_filter
        Secondary filtering prior to VCV and specific volatility predictions.
    
    """
    def __init__(self):
        filter.__init__(self)
                   
               
    def _initial_query_specification(self):
        # Note: The purpose of these is in large measure to ensure np.nan does not occur in numpy linear algebra routines (returns matrix of nan)
        # if we don't end up filtering out nan, then we need to have an appropriate filler. That is something to consider for future development.
        self.stk_queries.append("shrcd in [10,11]")
        self.stk_queries.append("ret not in [-66,-77,-88]")
        self.stk_queries.append("ret == ret")
        self.stk_queries.append("siccd == siccd") # should probably remove this and forward fill siccd when possible
        self.stk_queries.append("prc == prc") # maybe forward fill prices to not have more missing data?
        self.stk_queries.append("rf == rf") # probably forward fill this as well to ensure we don't have missing data 
        # TODO: queries based on fundamentals (append to self.fundamentals_queries)
        # TODO: any other queries needed
    
    @staticmethod
    def _volume_filter(g):
        sz = g.dolvol.size-1
        retval = g.dolvol.rank(method='max').apply(lambda x: (x-1)/sz)
        return retval

        
    def _illiquidity_specification(self,stk_db,quiet=True):
        if 'mcap' not in list(stk_db.columns): 
            stk_db['mcap'] = stk_db.prc * stk_db.shr * 1000.
        stk_db['year'] = stk_db.caldt.dt.year
        stk_db['month'] = stk_db.caldt.dt.month 
        mcappre = stk_db.groupby('caldt').mcap.sum()
        stk_db['dolvol'] = stk_db.vol * stk_db.prc
        dolvolmonth = stk_db.loc[(stk_db.year==2019)&(stk_db.month==12)].groupby('permno').dolvol.sum()
        percentile = percentileofscore(dolvolmonth,5.0e6,kind='weak')/100.
        monthly = pd.DataFrame(stk_db.groupby(['year','month','permno']).dolvol.sum())
        monthly['permno'] = monthly.index.get_level_values('permno')
        monthly['year'] = monthly.index.get_level_values('year')
        monthly['month'] = monthly.index.get_level_values('month')
        monthly = monthly.reset_index(drop=True)
        #monthly.dolvol = monthly.groupby('permno').dolvol.shift()
        monthly['volq'] = monthly.groupby(['year','month']).apply(self._volume_filter).reset_index(drop=True)
        stk_db = stk_db.merge(monthly,on=['permno','month','year'])
        stk_db = stk_db.query("volq > {}".format(percentile))
        mcappost = stk_db.groupby('caldt').mcap.sum()
        mcapcover = pd.concat([mcappre,mcappost],axis=1)
        mcapcover.columns=['pre','post']
        if not quiet:
            print('')
            print('percentile')
            print(percentile)
            print('')
            print('mcap coverage')
            print((mcapcover.post/mcapcover.pre).describe())
            print('')
        return stk_db
        
        