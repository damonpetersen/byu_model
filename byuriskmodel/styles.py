import pandas as pd
import numpy as np
from tabulate import tabulate

class style():
    """    
    Base style factor class for input to :code:`byuriskmodel.model.risk_model`. 
    The base class holds hidden methods for error checking, input checking, and cleaning 
    that would be redundant if held in child classes. Each child class should have a 
    :code:`_generate_exposures` method in which the style factor exposures for that 
    particular style factor is completed. 
    
    Parameters
    ----------

    None. 
        The base class is formed in such a way that the class takes no arguments 
        to instantiate, but has the methods to process :code:`byuriskmodel.model.risk_model.stk_db` 
        and :code:`byuriskmodel.model.risk_model.fundamentals_db` and return a corresponding :code:`DataFrame`
        containing security exposures to the give style factor.
    
    Methods
    --------
    
    generate_exposures(stk_db,fundamentals_db): 
        Return corresponding :code:`DataFrame` of exposures to given style factor.
    
    Notes
    ------
    
    The child class name must match the variable column name for the exposure.
    
    """

    def __init__(self):
        self.descriptor_weights = [1.]
        
    def _assertions(self,stk_db,fundamentals_db):
        assert isinstance(stk_db,pd.core.frame.DataFrame), "stk_db must be a pandas dataframe"
        assert isinstance(fundamentals_db,pd.core.frame.DataFrame)
        for c in ['caldt','ret','siccd','prc','shr','rf','permno']:
            assert c in list(stk_db.columns), "{} must be in stk_db".format(c)
        #for c in []
        #    assert c in list(fundamentals_db.columns), "{} must be in fundamentals_db".format(c)
        
    def _clean(self,stk_db):
        pass
    
    def generate_exposures(self,stk_db,fundamentals_db,extra=None):
        self._assertions(stk_db,fundamentals_db)
        # maybe clean?
        exposures = self._generate_exposures(stk_db,fundamentals_db,extra=extra)
        return exposures.reset_index(drop=True)
        
    def rebalance_weights(w, descriptors):
        descriptors = descriptors.to_numpy(descriptors)
        nans = np.argwhere(np.isnan(descriptors))
        new_w = w
        if np.size(nans) != 0:
            np.put(w, nans, 0)  
            np.nan_to_num(descriptors, 0)
            new_w = w/sum(w)
        return new_w
      
    @staticmethod
    def _exponential_weights(n,tau):
        # old to recent
        lamb = np.power(0.5,1./tau)
        df = pd.DataFrame(np.nan,index=np.arange(n),columns=['w'])
        df['T-t'] = n-pd.Series(df.index.to_list())
        df['w'] = np.power(lamb,df['T-t'])
        df['w'] = df['w'] / df.w.sum()
        return df[['w']]
        
    @staticmethod
    def _orthogonalize(base,new,mcap):
        # base is what we are orthogonalizing to and new is what we are orthogonalizing
        base = np.array(base)
        new = np.array(new)
        n = base.shape[0]
        base = np.reshape(base,(n,1))
        new = np.reshape(new,(n,1))
        mcapsv = np.sqrt(mcap)
        mcaps = np.diag(mcapsv)
        myinv = np.linalg.inv(np.matmul(base.T,np.matmul(mcaps,base)))
        orth = new - np.matmul(np.matmul(base,myinv),np.matmul(new.T,np.matmul(mcaps,base)))
        return orth
        
    def _wstd(self,s,tau):
        # weighted standard deviation
        s = pd.DataFrame(s).reset_index(drop=True)
        n = s.shape[0]
        s.columns=['main']
        s['w'] = self._exponential_weights(n,tau)
        wbar = (s.main * s.w).sum()
        s['tmp1'] = s.w * np.power(s.main - wbar,2)
        std = np.sqrt(s.tmp1.sum()*n / ((n-1) * s.w.sum()) )
        return std
        
    @staticmethod
    def _cumret(g,win,col='ret'):
        g = g.sort_values('caldt').reset_index(drop=True)
        n = g.shape[0]
        g['grouper'] = pd.Series(np.arange(12)).repeat(21).reset_index(drop=True)
        return g.groupby('grouper').apply(lambda x: (1.0+x[col]).prod()-1.0)
        
    @staticmethod
    def _standardize_2(col,vw):
        std = col.std(ddof=1)
        mu = (col*vw).mean()
        newcol = (col-mu)/std
        return newcol
        
    def _standardize(self,g,descriptors):
        # g needs to have an mcap column
        g['vw'] = g.mcap / g.mcap.sum()
        g[descriptors] = g[descriptors].apply(self._standardize_2,vw=g['vw'],axis=0)
        return g.drop('mcap',axis=1)
        
    @staticmethod
    def _winsorize(g,col,stds):
        # winsorize 'col' to 'stds' standard deviations away from mean 
        std = g[col].std(ddof=1)
        mu = g[col].mean()
        g.loc[g[col]>mu+stds*std,col] = mu+stds*std 
        g.loc[g[col]<mu-stds*std,col] = mu-stds*std
        return g[col]
        
    
class mktbeta(style):
    """    
    Market beta style factor. Returns both market beta exposures by security and time point 
    but also residuals from market beta exponential weighted least squares estimation for use in 
    residual volatility style factor formation.
    
    Parameters
    ----------

    None. 
    
    Methods
    --------
    
    generate_exposures(stk_db,fundamentals_db): 
        Returns :code:`DataFrame` of :code:`mktbeta` and :code:`mktbetaresid` by :code:`permno` and :code:`caldt`.
    
    
    """
    def __init__(self):
        style.__init__(self)
    
    def _generate_exposures(self,stk_db,fundamentals_db,extra=None):
        if 'mcap' not in list(stk_db.columns): 
            stk_db['mcap'] = stk_db.prc * stk_db.shr * 1000.
        stk_db['exret'] = stk_db.ret - stk_db.rf 
        stk_db['rme'] = stk_db['mcap']*stk_db['ret']
        mktvw = stk_db.pivot_table('rme','caldt',aggfunc='sum')['rme']
        x = stk_db.pivot_table('mcap','caldt',aggfunc='sum')['mcap']
        mktvw = mktvw.divide(x)
        mktvw = pd.DataFrame(mktvw)
        mktvw.columns = ['mktvw']
        stk_db = stk_db.merge(mktvw,on='caldt',how='left')
        stk_db['exmktvw'] = stk_db.mktvw - stk_db.rf
        mktbetas = stk_db.groupby('permno').apply(self._mktbeta_by_security)
        mktbetas = mktbetas.groupby('caldt').apply(self._standardize,['mktbeta'])
        #mktbetas['caldt'] = mktbetas.index.get_level_values('caldt')
        return mktbetas[['mktbeta','mktbetaresid','permno','caldt']]
    
    def _mktbeta_by_security(self,g):
        tau = 63 # days
        window = 252 # days
        g = g.sort_values('caldt').reset_index(drop=True)
        n = g.shape[0]
        g['mktbeta'] = np.nan
        g['mktbetaresid'] = np.nan
        g['const'] = 1
        for i in range(n):
            if i > window:
                df = g.iloc[i-window:i,:]
                X = df[['const','exmktvw']].to_numpy()
                W = np.diag(np.reshape(self._exponential_weights(window,tau).to_numpy(),window))
                Y = df['exret'].to_numpy()
                myinv = np.linalg.inv(np.matmul(X.T,np.matmul(W,X)))
                b = np.matmul(myinv,np.matmul(X.T,np.matmul(W,Y)))
                g.loc[i,'mktbeta'] = b[1]
                g.loc[i,'mktbetaresid'] = g.loc[i,'exret'] - b[0] - g.loc[i,'exmktvw'] * b[1]
        return g[['caldt','mktbeta','mktbetaresid']+['mcap','permno']]
            
class momentum(style):
    """    
    Momentum style factor. 
    
    Parameters
    ----------

    None. 
    
    Methods
    --------
    
    generate_exposures(stk_db,fundamentals_db): 
        Returns :code:`DataFrame` of :code:`momentum` by :code:`permno` and :code:`caldt`.
    
    
    """
    def __init__(self):
        style.__init__(self)
    
    def _generate_exposures(self,stk_db,fundamentals_db,extra=None):
        momentums = stk_db.groupby('permno').apply(self._momentum_by_security)
        momentums = momentums.groupby('caldt').apply(self._standardize,['momentum'])
        #momentums['caldt'] = momentums.index.get_level_values('caldt')
        return momentums[['momentum','caldt','permno']]

    def _momentum_by_security(self,g):
        L = 21 # days
        T = 504 # days
        tau = 126 # days
        n = g.shape[0]
        w = self._exponential_weights(T-L,tau)
        g = g.sort_values('caldt').reset_index(drop=True)
        g['momentum'] = np.nan
        for i in range(n):
            if i > (T+L):
                df = g.iloc[(i-(T+L)):(i-L),:]
                df['w'] = w 
                df['intermediate'] = df.w * ( np.log(1.0+df.ret) - np.log(1.0+df.rf) )
                g.loc[i,'momentum'] = df.intermediate.sum()
        return g[['momentum','caldt']+['mcap','permno']]
        
class size(style):
    """    
    Size style factor. 
    
    Parameters
    ----------

    None. 
    
    Methods
    --------
    
    generate_exposures(stk_db,fundamentals_db): 
        Returns :code:`DataFrame` of :code:`size` by :code:`permno` and :code:`caldt`.
    
    
    """
    def __init__(self):
        style.__init__(self)
    
    def _generate_exposures(self,stk_db,fundamentals_db,extra=None):
        stk_db = stk_db.copy()
        if 'mcap' not in list(stk_db.columns): 
            stk_db['mcap'] = stk_db.prc * stk_db.shr * 1000.
        stk_db['size'] = np.log(stk_db['mcap'])
        sizes = stk_db.groupby('caldt').apply(self._standardize,['size'])
        #sizes['caldt'] = sizes.index.get_level_values('caldt')
        return sizes[['size','caldt','permno']]
        
class residual_volatility(style):
    """    
    Residual volatility style factor. 
    
    Parameters
    ----------

    mktbetaresid: 
        :code:`DataFrame` containing residuals from :code:`mktbeta` class regression by :code:`permno` and :code:`caldt`.
    
    Methods
    --------
    
    generate_exposures(stk_db,fundamentals_db): 
        Returns :code:`DataFrame` of :code:`residual_volatility` by :code:`permno` and :code:`caldt`.
    
    
    """
    def __init__(self):
        style.__init__(self)
    
    def _generate_exposures(self,stk_db,fundamentals_db,extra=None):
        assert isinstance(extra,pd.core.frame.DataFrame), "extra must be mktbetaresid dataframe for exposure generation for residual volatiltity"
        assert 'mktbetaresid' in extra.columns, "extra must be mktbetaresid dataframe for exposure generation for residual volatiltity containing a 'mktbetaresid' column"
        stk_db = stk_db.merge(extra,on=['permno','caldt'])
        residual_volatilities = stk_db.groupby('permno').apply(self._residual_volatility_by_security)
        residual_volatilities = residual_volatilities.groupby('caldt').apply(self._standardize,['cmra','hsigma','dastd'])
        residual_volatilities.residual_volatility = 0.15 * residual_volatilities.cmra + 0.75 * residual_volatilities.dastd + 0.1 * residual_volatilities.hsigma
        #residual_volatilities['caldt'] = residual_volatilities.index.get_level_values('caldt')
        return residual_volatilities[['permno','caldt','residual_volatility']].query("permno == permno")

    def _residual_volatility_by_security(self,g):
        tau_dastd = 42 # days
        tau_hsigma = 63 # days
        window = 252 # days
        month = 21 # days
        g['residual_volatility'] = np.nan
        g['cmra'] = np.nan 
        g['hsigma'] = np.nan
        g['dastd'] = np.nan
        n = g.shape[0]
        for i in range(n):
            if i > window:
                df = g.iloc[i-window:i,:]
                hsigma = self._wstd(df['mktbetaresid'],tau_hsigma)
                cumrets = self._cumret(df,month,col='ret')
                cumrfs = self._cumret(df,month,col='rf')
                Z = np.zeros(12)
                for j in range(12):
                    if j == 0:
                        Z[j] = np.log(1.0+cumrets.iloc[-1]) - np.log(1.0+cumrfs.iloc[-1])
                    else:
                        Z[j] = Z[j-1] + np.log(1.0+cumrets.iloc[-(j+1)]) - np.log(1.0+cumrfs.iloc[-(j+1)])
                cmra = np.log(1.0+max(Z)) - np.log(1.0+min(Z))
                dastd = self._wstd(df['exret'],tau_dastd)
                rv = 0.75 * dastd + 0.15 * cmra + 0.1 * hsigma
                if rv == np.nan:
                    debug = True
                g.loc[i,'residual_volatility'] = rv
                g.loc[i,'cmra'] = cmra 
                g.loc[i,'hsigma'] = hsigma 
                g.loc[i,'dastd'] = dastd
        return g[['caldt','residual_volatility','cmra','hsigma','dastd']+['mcap','permno']]

class liquidity(style):
    """    
    Liquidity style factor. 
    
    Parameters
    ----------

    None.
    
    Methods
    --------
    
    generate_exposures(stk_db,fundamentals_db): 
        Returns :code:`DataFrame` of :code:`liquidity` by :code:`permno` and :code:`caldt`.
    
    
    """
    def __init__(self):
        style.__init__(self)
    
    def _generate_exposures(self,stk_db,fundamentals_db,extra=None):
        liquidities = stk_db.groupby('permno').apply(self._liquidity_by_security)
        liquidities = liquidities.groupby('caldt').apply(self._standardize,['stom','stoq','stoa'])
        liquidities.liquidity = 0.35 * liquidities.stom + 0.35 * liquidities.stoq + 0.3 * liquidities.stoa 
        #liquidities['caldt'] = liquidities.index.get_level_values('caldt')
        return liquidities[['permno','caldt','liquidity']].query("permno == permno")

    def _liquidity_by_security(self,g):
        window_stom = 21 # days
        window_stoq = 21*3 # days 
        window_stoa = 21*12 # days
        n = g.shape[0]
        g['liquidity'] = np.nan
        g['stom'] = np.nan
        g['stoq'] = np.nan
        g['stoa'] = np.nan
        for i in range(n):
            if i > window_stoa:
                stoms = np.zeros(12)
                for j in range(12):
                    df = g.iloc[i-window_stom*(j+1):i-window_stom*j,:]
                    stoms[j] = np.log((df.vol/(df.shr*1000.)).sum())
                stom = stoms[0]
                stoq = np.log(1./3.*np.exp(stoms[:3]).sum())
                stoa = np.log(1./12.*np.exp(stoms).sum())
                liq = 0.35 * stom + 0.35 * stoq + 0.3 * stoa 
                g.loc[i,'liquidity'] = liq 
                g.loc[i,'stom'] = stom 
                g.loc[i,'stoq'] = stoq
                g.loc[i,'stoa'] = stoa 
        return g[['caldt','liquidity','stom','stoq','stoa']+['mcap','permno']].reset_index(drop=True)

class nonlinear_beta(style):
    """    
    Non-linear market beta factor. 
    
    Parameters
    ----------

    mktbeta: 
        :code:`mktbeta` exposures by :code:`permno` and :code:`caldt` from :code:`mktbeta` class.
    
    Methods
    --------
    
    generate_exposures(stk_db,fundamentals_db): 
        Returns :code:`DataFrame` of :code:`nonlinear_beta` by :code:`permno` and :code:`caldt`.
    
    
    """
    def __init__(self):
        style.__init__(self)
    
    def _generate_exposures(self,stk_db,fundamentals_db,extra=None):
        assert isinstance(extra,pd.core.frame.DataFrame), "extra must be mktbeta dataframe for exposure generation for nonlinear_beta"
        assert 'mktbeta' in extra.columns, "extra must be mktbeta dataframe for exposure generation for nonlinear_beta containing a 'mktbeta' column"
        stk_db = stk_db.merge(extra,on=['permno','caldt'])
        if 'mcap' not in list(stk_db.columns): 
            stk_db['mcap'] = stk_db.prc * stk_db.shr * 1000.
        nonlinear_betas = stk_db.groupby('caldt').apply(self._nonlinear_beta_by_date)
        #nonlinear_betas['caldt'] = nonlinear_betas.index.get_level_values('caldt')
        nonlinear_betas = nonlinear_betas.groupby('caldt').apply(self._standardize,['nonlinear_beta'])
        return nonlinear_betas[['permno','caldt','nonlinear_beta']]
    
    def _nonlinear_beta_by_date(self,g):
        g['mktbeta3'] = np.power(g['mktbeta'],3)
        g['nonlinear_beta'] = self._orthogonalize(g['mktbeta3'],g['mktbeta'],g['mcap'])
        g['nonlinear_beta'] = self._winsorize(g,'nonlinear_beta',3.0)
        g['caldt'] = g.name
        return g[['nonlinear_beta','caldt']+['mcap','permno']]


class nonlinear_size(style):
    """    
    Non-linear size factor. 
    
    Parameters
    ----------

    size: 
        :code:`size` exposures by :code:`permno` and :code:`caldt` from :code:`size` class.
    
    Methods
    --------
    
    generate_exposures(stk_db,fundamentals_db): 
        Returns :code:`DataFrame` of :code:`nonlinear_size` by :code:`permno` and :code:`caldt`.
    
    
    """
    def __init__(self):
        style.__init__(self)
    
    def _generate_exposures(self,stk_db,fundamentals_db,extra=None):
        assert isinstance(extra,pd.core.frame.DataFrame), "extra must be size dataframe for exposure generation for nonlinear_size"
        assert 'size' in extra.columns, "extra must be mktbeta dataframe for exposure generation for nonlinear_size containing a 'size' column"
        stk_db = stk_db.merge(extra,on=['permno','caldt'])
        if 'mcap' not in list(stk_db.columns): 
            stk_db['mcap'] = stk_db.prc * stk_db.shr * 1000.
        nonlinear_sizes = stk_db.groupby('caldt').apply(self._nonlinear_sizes_by_date)
        #nonlinear_sizes['caldt'] = nonlinear_sizes.index.get_level_values('caldt')
        nonlinear_sizes = nonlinear_sizes.groupby('caldt').apply(self._standardize,['nonlinear_size'])
        return nonlinear_sizes[['nonlinear_size','permno','caldt']]
    
    def _nonlinear_sizes_by_date(self,g):
        g['size3'] = np.power(g['size'],3)
        g['nonlinear_size'] = self._orthogonalize(g['size3'],g['size'],g['mcap'])
        g['nonlinear_size'] = self._winsorize(g,'nonlinear_size',3.0)
        g['caldt'] = g.name
        return g[['nonlinear_size','caldt']+['mcap','permno']]
